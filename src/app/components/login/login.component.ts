import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/bonus.service';
import { AuthService } from '../auth/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  public loginForm!: FormGroup;
  loading: boolean = false;
  public userEmail = localStorage.getItem('user')?.toString();

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private apiService: ApiService,
    private http: HttpClient,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
    this.onSubmit();
  }

  getData() {}

  get formControls() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.loading = true;

    if (!this.userEmail) {
      if (this.loginForm.valid) {
        this.spinner.show();

        this.apiService
          .makeApiRequest('POST', 'loginUser', this.loginForm.value)
          .subscribe((res) => {
            this.authService.login(this.loginForm.value.email);
            this.router.navigate(['/dashboard']);
            this.spinner.hide();
            localStorage.setItem('user', this.loginForm.value.email);
            localStorage.setItem('name', res.data.name);
          });
      } else {
        console.log('Form is invalid');
      }
    } else {
      this.authService.login(this.userEmail);
      this.router.navigate(['/dashboard']);
    }
  }
}
