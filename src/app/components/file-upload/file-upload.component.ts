import { Component } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/bonus.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent {
  fileName!:string;
  constructor(private toastr: ToastrService,
    private apiService: ApiService,
    private spinner: NgxSpinnerService){

  }
  onFileSelected(event: any): void {
    const file: File = event.target.files[0];
    this.uploadFile(file);
    this.fileName = file.name
    console.log(file)
  }
  uploadFile(file: File): void {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    this.spinner.show();
      this.apiService.makeApiRequest('POST', 'products', formData).subscribe((res) => {
        if (res) {
          this.spinner.hide();
          this.toastr.success('Success','File Upload Successfully', {
            timeOut: 3000,
          });
        }
      });
  

  }
}
