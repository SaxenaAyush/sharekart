import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/bonus.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent {
  public reportList: any;
  userId = 45;
  itemsPerPage: number = 5;
  currentPage: number = 1;
  totalItems: number = 100;

  constructor(private apiService: ApiService) {}

  public ngOnInit(): void {
    this.getTransaction();
  }

  getTransaction() {
    const req = {
      userId: this.userId,
      page: this.currentPage,
      limit: this.itemsPerPage,
    };
    // this.apiService.makeRequestApi('post', 'transactions',req).subscribe((res) => {
    //   this.reportList = res;
    // });
  }

  onPageChange(event: any) {
    this.currentPage = event.pageIndex + 1;
    this.itemsPerPage = event.pageSize;
  }
}
