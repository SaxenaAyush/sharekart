import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/bonus.service';

@Component({
  selector: 'app-referals',
  templateUrl: './referals.component.html',
  styleUrls: ['./referals.component.scss'],
})
export class ReferalsComponent {
  displayedColumns: string[] = [
    'parent_id',
    'Name',
    'phone_number',
    'email_id',
    'date_of_referal',
    'status',
    'date_of_mvp',
  ];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  dataSource!: MatTableDataSource<any>;

  public referalsList: any;
  itemsPerPage: number = 25;
  currentPage: number = 1;
  totalItems: number = 100;
  inputValue: string = '';
  toDate: string = '';
  fromDate: string = '';
  pageSizeOptions: number[] = [25, 50, 100];

  constructor(
    private apiService: ApiService,
    private spinner: NgxSpinnerService
  ) {}

  public ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    const req = {
      page: this.currentPage,
      limit: this.itemsPerPage,
      search: this.inputValue,
    };
    this.spinner.show();
    this.apiService.makeApiRequest('POST', 'referals', req).subscribe((res) => {
      if (res) {
        this.spinner.hide();
        this.totalItems = res.data.totalItems;
        this.itemsPerPage = this.pageSizeOptions[0];
        this.dataSource = new MatTableDataSource(res.data.referals);
        this.dataSource.sort = this.sort;
      }
    });
  }

  onPageChange(event: any) {
    this.currentPage = event.pageIndex + 1;
    this.itemsPerPage = event.pageSize;
    this.getProducts();
  }

  applyFilter() {
    this.currentPage = 1;
    this.getProducts();
  }

  onSortChange(event: Sort): void {
    if (event.active === 'parent_id') {
      const sortDirection = event.direction === 'asc' ? 'desc' : 'asc';

      const sortString =  sortDirection;
      const req = {
        page: this.currentPage,
        limit: this.itemsPerPage,
        search: this.inputValue,
        parentSort: sortString
      };
      this.spinner.show();
      this.apiService.makeApiRequest('POST', 'referals', req).subscribe((res) => {
        if (res) {
          this.spinner.hide();
          this.dataSource = new MatTableDataSource(res.data.referals);
          this.totalItems = res.data.totalItems;
        }
      });
    }
  }
}
