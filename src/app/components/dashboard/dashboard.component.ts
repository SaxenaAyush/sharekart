import { Component } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/bonus.service';
import { ChartOptions } from 'chart.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  dasboardData: any;
  totalSale: any;
  totalUsers: any;
  totalOrders: any;
  activeReferralUsers: any;
  avgOrderValue: any;
  topSellingProduct: any;
  averageUserRewardPointsAllocated: any;

  public pieChartOptions: ChartOptions<'pie'> = {
    responsive: false,
  };
  public pieChart1Labels!: any;
  public pieChart1Datasets = [
    {
      data: [300, 500, 100],
    },
  ];
  public pieChart2Labels!: any;
  public pieChart2Datasets = [
    {
      data: [300, 500, 100],
    },
  ];
  public pieChartLegend = true;
  public pieChartPlugins = [];

  constructor(
    private apiService: ApiService,
    private spinner: NgxSpinnerService
  ) {}

  public ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.spinner.show();
    this.apiService.makeApiRequest('POST', 'dashboard').subscribe((res) => {
      if (res) {
        this.spinner.hide();
        this.dasboardData = res.data;
        const char1keys = [];
        const chart1values = [];
        const char2keys = [];
        const chart2values = [];
        for (const item of res.data.pieChart1) {
          const key = Object.keys(item)[0];
          const value = item[key];

          char1keys.push(key);
          chart1values.push(value);
        }
        for (const item of res.data.pieChart2) {
          const key = Object.keys(item)[0];
          const value = item[key];

          char2keys.push(key);
          chart2values.push(value);
        }
        this.chartValues(res);
        this.pieChart1Labels = char1keys;
        this.pieChart1Datasets = [
          {
            data: chart1values,
          },
        ];
        this.pieChart2Labels = char2keys;
        this.pieChart2Datasets = [
          {
            data: chart2values,
          },
        ];
        this.totalSale = res.data.sales;
        this.totalOrders = res.data.totalOrders;
        this.totalUsers = res.data.totalUsers;
        this.activeReferralUsers = res.data.activeReferralUsers;
        this.avgOrderValue = res.data.avgOrderValue;
        this.averageUserRewardPointsAllocated =
          res.data.averageUserRewardPointsAllocated;
      }
    });
  }
  chartValues(res: any) {
    const keys = [];
    const values = [];
    for (const item of res.data.pieChart1) {
      const key = Object.keys(item)[0];
      const value = item[key];

      keys.push(key);
      values.push(value);
    }
  }
}
