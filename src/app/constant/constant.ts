import { environment } from './../../environments/environment';

export class ConstantsHelper {
  public static readonly getAPIUrl = {
    loginUser: () => `https://www.lifemobilitysolutions.in/admin/login`,
    transactions: () => `${environment.baseUrl}/admin/transactions`,
    users: () => `${environment.baseUrl}/admin/users`,
    products: () => `${environment.baseUrl}/admin/products`,
    referals: () => `${environment.baseUrl}/admin/referals`,
    billing: () => `${environment.baseUrl}/billing/records`,
    downloadBilling: () => `${environment.baseUrl}/billing/records/download`,
    downloadTransaction: () => `${environment.baseUrl}/admin/transactions/download`,
    raiseFlag: () => `${environment.baseUrl}/billing/records/flag`,
    dashboard: () => `${environment.baseUrl}/dashboard`,
    fileUpload: () => `${environment.baseUrl}/uploadFile`,

  };
}
