import {
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
  NgModule,
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeLayoutComponent } from './components/layout/home-layout/home-layout.component';
import { LoginLayoutComponent } from './components/layout/login-layout/login-layout.component';
import { AuthGuard } from './components/auth/authGuard';
import { AuthService } from './components/auth/auth.service';
import { ViewProductComponent } from './components/products/view-product/view-product.component';
import { ReportsComponent } from './components/reports/reports.component';
import { TransactionComponent } from './components/transaction/transaction.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ApiService } from './services/bonus.service';
import { HttpClientModule } from '@angular/common/http';
import { UsersDataComponent } from './components/users-data/users-data.component';
import { ReferalsComponent } from './components/referals/referals.component';

import { RouterModule } from '@angular/router';

import { FooterComponent } from './components/footer/footer.component';
import { MaterialModel } from './components/material/material.model';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BillingComponent } from './components/billing/billing.component';
import { NgChartsModule } from 'ng2-charts';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import {ToastrModule} from 'ngx-toastr'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    SidebarComponent,
    DashboardComponent,
    HomeLayoutComponent,
    LoginLayoutComponent,
    ViewProductComponent,
    ReportsComponent,
    TransactionComponent,
    UsersDataComponent,
    ReferalsComponent,
    FooterComponent,
    BillingComponent,
    FileUploadComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    RouterModule,
    HttpClientModule,
    MaterialModel,
    NgxSpinnerModule,
    NgChartsModule,
    ToastrModule.forRoot(),
  ],
  providers: [AuthService, AuthGuard, ApiService, HttpClientModule],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class AppModule {}
